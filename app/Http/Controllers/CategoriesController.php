<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CategoriesRequest;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoriesController extends Controller
{
    private $category;
    /**
     * Construct
    */
    public function __construct(Categories $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $data = [];
        
        // $categories = DB::select('select * from categories');

        $categories = $this->category;

        // Check have Search ?
        if ($request->name) {
            $categories = $categories->where('name', 'like', '%' . $request->name . '%');
        }

        // $categories = $categories->count('name');

        $categories = $categories->get();
        
        $data['categories'] = $categories; 
        // dd($data);

        return view('categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return view('categories.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {
        $categoryInsert = [
            'name' => $request->category_name
        ];

        try {
            // insert into data to table category (successful)
            $this->category->create($categoryInsert);
            return redirect()->route('category.index')->with('success', trans('message.store_category_success'));

        } catch (\Exception $ex) {
            //insert into data to table category (fail)
            Log::error($ex->getMessage());
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show(Categories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $category = $this->category->findOrFail($id);
        $data['category'] = $category;

        return view('categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesRequest $request, $id)
    {
        try {
            $category = $this->category->findOrFail($id);

            $category->name = $request->category_name;
            $category->save();

            return redirect()->route('category.index')->with('success', trans('message.update_category_success'));
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = $this->category->find($id);
            $category->delete();
            return redirect()->route('category.index')->with('success', trans('message.delete_category_success'));
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', $ex->getMessage());
        }
        
    }
}