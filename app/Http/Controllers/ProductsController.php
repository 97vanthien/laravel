<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductRequest;
use App\Models\Categories;
use App\Models\Products;
use App\Utils\UploadFile;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $folderThumb;
    private $folderImage;
    protected $product;
    protected $category;

    public function __construct(Products $product, Categories $category)
    {
        $this->category = $category;
        $this->product = $product;

        // Set Path for folder Upload File
        $this->folderThumb = config('common.folder_product_thumb_upload');
        $this->folderImage = config('common.folder_product_img_upload');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        // Set Sort Data
        $orderBy = $request->get('order_by', 'id');
        $sortedBy = $request->get('sorted_by', 'desc');

        // Set Pagiantion Limit in per page
        $perPage = $request->get('per_page', config('common.pagination_limit_10'));

        // Set Field List
        $fields = [
            'products.category_id',
            'categories.name as category_name',
            'products.id',
            'products.name',
            'products.thumbnail',
            'products.price',
        ];

        // Get list data of table products
        $products = $this->product
            ->select($fields)
            ->leftJoin('categories', 'products.category_id', '=', 'categories.id');

        // Search with keyword
        if (!empty($request->keyword)) {
            $products = $products->where('products.name', 'like', '%' . $request->keyword . '%');
        }

        // Search category_id
        if (!empty($request->category_id)) {
            $products = $products->where('products.category_id', $request->category_id);
        }

        // Sort Data
        $products = $products->orderBy($orderBy, $sortedBy);

        // Pagination
        $products = $products->paginate($perPage);
        $data['products'] = $products;

        // Get list data of table categories
        $categories = $this->category->pluck('name', 'id')
            ->toArray();
        $data['categories'] = $categories;
        return view('products.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        // Get Category List
        $categories = $this->category->pluck('name', 'id')
            ->toArray();
        $data['categories'] = $categories;

        return view('products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        // Upload Thumbnail
        $thumbnailPath = UploadFile::upload($request->thumbnail, $this->folderThumb);

        // Upload Images
        $files = $request->images;
        $productImages = UploadFile::upload($files, $this->folderImage);
        $productImages = empty($productImages) ? [] : $productImages;

        // Define Variable
        $dataInsert = [
            'name' => $request->name,
            'thumbnail' => $thumbnailPath,
            'description' => $request->description,
            'content' => $request->content,
            'quantity' => $request->quantity,
            'price' => $request->price,
            'images' => json_encode($productImages),
            'category_id' => $request->category_id,
        ];

        try {
            // insert into table products
            $this->product->create($dataInsert);

            // success
            return redirect()->route('products.index')
                ->with('success', 'Insert successful!');
        } catch (\Exception $ex) {
            return redirect()->back()
                ->with('error', $ex->getMessage())
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Define Variable
        $data = [];

        // Get Product Detail
        $product = $this->product
            ->findOrFail($id);
        $data['product'] = $product;

        return view('products.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        // Get Product Detail
        $product = $this->product
            ->findOrFail($id);
        $data['product'] = $product;

        // Get Category List
        $categories = $this->category->pluck('name', 'id')
            ->toArray();
        $data['categories'] = $categories;

        return view('products.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get Product
        $product = $this->product
            ->findOrFail($id);

        try {
            // delete data of table products
            $product->delete();

            // success
            return redirect()->route('products.index')
                ->with('success', 'Delete successful!');
        } catch (\Exception $ex) {
            return redirect()->back()
                ->with('error', $ex->getMessage());
        }
    }
}
