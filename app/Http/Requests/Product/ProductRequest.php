<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|integer',
            'name' => 'required|min:5|max:255|unique:products',
            'thumbnail' => 'required|image|mimes:jpg,jpeg,png,gif|max:10240', // size:10240 (10 MB)
            'description' => 'required',
            'content' => 'nullable',
            'quantity' => 'required|integer|min:1|max:9999',
            'price' => 'required|min:1|max:999999999',
            'images' => 'nullable|array',
            'images.*' => 'nullable|image|mimes:jpg,jpeg,png,gif|max:10240', // size:10240 (10 MB)
        ];
    }

    public function attributes()
    {
        return [
            'category_id' => 'Category ID',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
