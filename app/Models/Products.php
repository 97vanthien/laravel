<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'products';

    protected $fillable = [

        'category_id',
        'name',
        'thumbnail',
        'description',
        'content',
        'status',
        'quantily',
        'price',
        'images',
    ];

    public $timestamps = true;

    // protected $hidden = [

    // ];

    // public const STATUS = [
    //     0,  //hidden
    //     1, //show
    // ];

    // public const PAGE_LIMIT = 10;

    /**
     * Get the category that owns the post.
     */
    public function category()
    {
        return $this->belongsTo(Categories::class, 'category_id', 'id');
    }

}
