<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Categories extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = [

        'name',
        'slug',
        'status',

    ];

    public $timestamps = true;

    /**
     * Define Status
     * 
     * @value: 1 (show)
     * @value: 0 (hidden)
     */
    public const STATUS = [
        0,  //hidden
        1,  //show
    ];

    public static function boot(){
        parent::boot();

        // update data for field slug of table categories
        static::saving(function ($model){
            $slug = Str::of($model->name)->slug('-');
            $model->slug = strtolower($slug);
        });
    }
    
    /**
     * The "booted" method of the model.
     * 
     * @Return void
     * 
     * hien danh sach co status = 1
     */

     protected static function booted(){
         static::addGlobalScope('scope_status', function (Builder $builder){
            $builder->where('categories.status', self::STATUS[1]);
         });
     }

     /**
      * 1-n
      * Get the posts for the category
      */

     public function products(){
         
         return $this->hasMany(Products::class);
     }
}
