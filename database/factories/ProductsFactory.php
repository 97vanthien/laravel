<?php

namespace Database\Factories;

use App\Models\Categories;
use App\Models\Products;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Products::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $cate = Categories::pluck('id')->toArray();
        return [
            'category_id' => $this->faker->randomElement($cate),
            'name' => $this->faker->unique()->name(),   
            'thumbnail' => $this->faker->text('50'),
            'description' => $this->faker->text('50'),
            'content' => $this->faker->paragraph(),
            'status' => (bool)rand(0,1),
            'quantily' => (bool)rand(0,1),
            'price' => $this->faker->numberBetween(500000,2000000),
        ];
    }
}
