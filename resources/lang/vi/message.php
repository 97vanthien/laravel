<?php

return [
    // Label
    'product_name' => 'Tên sản phẩm',
    'thumbnail' => 'Ảnh đại diện',
    'price' => 'Giá',
    'quantity' => 'Số lượng',
    'money' => 'Số tiền',
    'action' => 'Thao tác',
    'update' => 'Cập nhật',
    'remove' => 'Xóa',
    'summary' => 'Tổng',
    'close' => 'Đóng',
    'list' => 'Danh sách',
    'create' => 'Thêm mới',
    'detail' => 'Chi tiết',
    'edit' => 'Chỉnh sửa',
    'delete' => 'Xóa',
    'search' => 'Tìm kiếm',
    'reset' => 'Reset',
    'category' => 'Danh mục',
    'home_page' => 'Trang chủ',
    
    // Product List
    'category_name' => 'Tên danh mục',
    'create_product' => 'Thêm sản phẩm',
    'search_product' => 'Tìm kiếm sản phẩm',
    'detail_product' => 'Xem chi tiết sản phẩm',
    'edit_product' => 'Chính sửa sản phẩm',
    'delete_product' => 'Xóa sản phẩm',
    'record_per_page' => 'bản ghi ở mỗi trang',
    'not_found_data' => 'Không tìm thấy dữ liệu.',
    'confirm_delete_product' => 'Bạn có chắc chắn muốn XÓA sản phẩm này ?',
    'show_per_page' => 'Hiển thị từ :from đến :to trong tổng số :total bản ghi',
    'product_management' => 'Quản lý sản phẩm',
    'product_image' => 'Danh sách hình ảnh',
    'product_description' => 'Mô tả sản phẩm',
    'product_content' => 'Nội dung sản phẩm',
    'required' => 'Không được để trống.',
    'save' => 'Lưu',
    'product_code' => 'Mã sản phẩm',

    // Category List
    'category_list' => 'Danh sách danh mục',
    'detail_category' => 'Chi tiết danh mục',
    'view_product_list' => 'Xem danh sách sản phẩm',
    'confirm_delete' => 'Bạn có chắc chắn muốn XÓA ?',
    'display_total_record' => 'Tổng: :total bản ghi',
    'edit_category' => 'Chỉnh sửa danh mục',
    'store_category_success' => 'Thêm mới danh mục thành công.',
    'update_category_success' => 'Cập nhật danh mục thành công.',
    'delete_category_success' => 'Xóa danh mục thành công.',
    'delete_category_fail' => 'Không thể xóa danh mục này vì nó đang được sử dụng ở sản phẩm.',

    // Search
    'keyword' => 'Từ khóa',
];
