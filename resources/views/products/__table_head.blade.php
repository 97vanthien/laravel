@php
// Sort Product Name (Desc)
$sortProductNameDesc = [
    'order_by' => 'name',
    'sorted_by' => 'desc',
];

// Sort Product Name (Asc)
$sortProductNameAsc = [
    'order_by' => 'name',
    'sorted_by' => 'asc',
];
@endphp

<thead class="bg-info">
    <tr>
        <th class="text-center">#</th>
        <th>{{ __('message.product_name') }}
            <div class="d-inline-block">
                <div class="control-sort">
                    @if(request()->order_by == 'name' && request()->sorted_by == 'asc')
                        <a href="{{ route('products.index', array_merge($paramRequest, $sortProductNameDesc)) }}"><i class="fas fa-sort-down"></i></a>
                    @elseif(request()->order_by == 'name' && request()->sorted_by == 'desc')
                        <a href="{{ route('products.index', array_merge($paramRequest, $sortProductNameAsc)) }}"><i class="fas fa-sort-up"></i></a>
                    @else
                        <a href="{{ route('products.index', array_merge($paramRequest, $sortProductNameAsc)) }}"><i class="fas fa-sort-up"></i></a>
                        <a href="{{ route('products.index', array_merge($paramRequest, $sortProductNameDesc)) }}"><i class="fas fa-sort-down"></i></a>
                    @endif
                </div>
            </div>
        </th>
        <th>{{ __('message.thumbnail') }}</th>
        <th>{{ __('message.price') }}</th>
        <th>{{ __('message.category_name') }}</th>
        <th colspan="4">{{ __('message.action') }}</th>
    </tr>
</thead>
