@php
    // Get Param Request
    $paramRequest = request()->except('page');
@endphp

@extends('layouts.master')

{{-- set page title --}}
@section('title', __('message.product_list'))

{{-- set breadcrumbName --}}
@section('breadcrumbName', __('message.product_management'))

{{-- set breadcrumbMenu --}}
@section('breadcrumbMenu', __('message.product_list'))

{{-- import file css (private) --}}
@push('css')
    <link rel="stylesheet" href="/css/product.css">
@endpush

{{-- import file js (private) --}}
@push('js')
    <!-- add jquery custom -->
    <script src="/js/product.js"></script>
@endpush

@section('content')
    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif

    {{-- form search --}}
    @include('products._search')

    {{-- display list product table --}}
    @include('products._table')
@endsection