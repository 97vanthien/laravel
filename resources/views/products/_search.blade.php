@php
    $categorySearch = request()->get('category_id') ? request()->get('category_id') : null;
@endphp

<!-- SHARING -->
<!-- jQuery Plugin Select2: https://select2.github.io/select2/ -->

<div class="mb-5 border p-3 bg-white section-search">
    <form action="{{ route('products.index') }}" method="GET" id="frm-product-search">
        <input type="hidden" name="per_page" value="10" id="per-page">
        <div class="row">
            <div class="col-md-6">
                <div class="mb-3">
                    <label class="form-label">{{ __('message.keyword') }}</label>
                    <input type="text" class="form-control" name="keyword" placeholder="{{ __('message.keyword_product_search_ph') }}" value="{{ request()->get('keyword') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="mb-3">
                    <label class="form-label">{{ __('message.category') }}</label>
                    <select name="category_id" class="form-control category-select2">
                        <option value=""></option>
                        @if(!empty($categories))
                            @foreach ($categories as $categoryId => $categoryName)
                                <option value="{{ $categoryId }}" {{ $categoryId == $categorySearch ? 'selected' : ''  }}>{{ $categoryName }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    
        <p>
            <button type="button" class="btn btn-secondary" onclick="window.location.href='{{ route('products.index') }}'"><i class="fas fa-sync-alt"></i> {{ __('message.reset') }}</button>
            <button type="submit" class="btn btn-primary" title="{{ __('message.search_product') }}"><i class="fas fa-search"></i> {{ __('message.search') }}</button>
        </p>
    </form>
</div>
