@extends('layouts.master')

@section('title', __('message.category_list'))

@section('content')
    @include('categories._search')

        {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif

    {{-- display list category table --}}

    @if ($categories->isEmpty())
        <p class="red">{{ __('message.not_found_data') }}</p>
    @else
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th>{{ __('message.category_name') }}</th>
                {{-- <th>{{ __('message.product_list') }}</th> --}}
                <th colspan="2">{{ __('message.action') }}</th>

            </tr>
            </thead>
            <tbody>
                @foreach ($categories as $key => $item)
                    <tr>
                        <th scope="row">{{ $key+1 }}</th>
                        <td>{{ $item->name }}</td>
                        <td>
                            <a href="{{ route('category.edit', $item->id) }}">
                                <button type="button" class="btn btn-warning">{{ __('message.edit') }}</button>
                        </td>
                        <td>       
                                                
                            <form action="{{ route('category.destroy', $item->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('{{ __('message.confirm_delete') }}')"><i class="fas fa-trash-alt"></i> {{ __('message.delete') }}</button>
                            </form>
                        </td>
                    </tr>      
                    
                @endforeach
            </tbody>
        </table>
    @endif

@endsection