<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title', __('message.home_page'))</title>
    {{-- css --}}
    @include('layouts.css')
</head>
<body>
    {{-- header --}}
    @include('layouts.header')

    {{-- menu --}}
    @include('layouts.menu')

    {{-- content --}}
    <main>
        <div class="container">
            @yield('content')
        </div>
    </main>

    {{-- footer --}}
    @include('layouts.footer')

    {{-- js --}}
    @include('layouts.js')
</body>
</html>
