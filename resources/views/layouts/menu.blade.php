<nav>
    <div class="container">
        <ul>
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('category.index') }}">Category List</a></li>
            <li><a href="{{ route('products.index') }}">Product List</a></li>
        </ul>
    </div>
</nav>
